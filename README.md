# Quotes

Collection of quotes.
I use it for my matrix bots… :)

## Origin

* bismarck.txt: https://www.quotez.net/german/otto_von_bismarck.htm
* stallman.txt: https://www.brainyquote.com/lists/authors/top-10-richard-stallman-quotes
* torvalds.txt: https://www.goodreads.com/author/quotes/92867.Linus_Torvalds
