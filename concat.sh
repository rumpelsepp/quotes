#!/usr/bin/env bash

rm quotes.txt

for f in *.txt; do
        name="$(basename "$f")"
        name="${name%.*}"
        awk -v name="$name" '{printf "%s: %s\n", name, $0}' "$f" >> quotes.txt
done
